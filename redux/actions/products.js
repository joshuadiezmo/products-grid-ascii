import { PRODUCTS } from "./actionTypes";
import { DEFAULT_LIMIT } from "../../utils/contants";
import { axiosInstance } from "../store";

const fetchProducts = (params = {}) => {
  const { _page = 1, _limit = DEFAULT_LIMIT, _sort } = params;
  const data = { _page, _limit, _sort };
  return async (dispatch) => {
    dispatch({ type: PRODUCTS.REQUEST, payload: { ...data } });
    try {
      const response = await axiosInstance.get("/products", { params: data });
      dispatch({ type: PRODUCTS.SUCCESS, payload: { data: response.data, ...data } });
    } catch (err) {
      dispatch({ type: PRODUCTS.FAILED, payload: err });
    }
  };
};

function generateAds() {
//    code here for some redux actions
}

export {
  fetchProducts,
  generateAds,
};
