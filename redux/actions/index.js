import { fetchProducts, generateAds } from "./products";

export {
  fetchProducts,
  generateAds,
};
