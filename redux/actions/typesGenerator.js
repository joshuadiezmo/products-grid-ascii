const TypesGenerator = type => ({
  REQUEST: `${type}_REQUEST`,
  SUCCESS: `${type}_SUCCESS`,
  FAILED: `${type}_FAILED`,
});

export default TypesGenerator;
