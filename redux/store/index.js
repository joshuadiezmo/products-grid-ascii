import { applyMiddleware, createStore, compose } from "redux";
import thunk from "redux-thunk";
import axios from "axios";
import reducers from "../reducers";
import { SERVER } from "../../utils/contants";

const axiosInstance = axios.create({
  baseURL: SERVER,
});

function reduxStore(initialState) {
  const store = createStore(reducers, initialState, compose(
    applyMiddleware(thunk),
  ));

  return store;
}
export {
  axiosInstance,
};
export default reduxStore;
