import { PRODUCTS } from "../actions/actionTypes";

const initialState = { requesting: false, list: [], error: null };

function reducer(state = initialState, action) {
  const nextState = Object.assign({}, state);

  switch (action.type) {
    case PRODUCTS.REQUEST: {
      const { _limit, _page, _sort } = action.payload;
      let { list } = nextState;
      if (_page === 1) list = [];
      return {
        ...nextState, requesting: true, error: null, _limit, _page, list, _sort,
      };
    }

    case PRODUCTS.SUCCESS: {
      const { data, _limit = 20, _sort } = action.payload;

      let nextPage;
      let { _page = 1 } = action.payload;
      let { list = [] } = nextState;

      if (data.length === _limit) {
        nextPage = _page + 1;
      } else {
        _page -= 1;
      }

      if (_page === 1) {
        list = [];
      }
      list.push(...data);

      return {
        ...nextState, requesting: false, list, _limit, _page, nextPage, _sort,
      };
    }

    case PRODUCTS.FAILED: {
      const { message } = action.payload;
      const error = message && typeof message === "string" ? new Error(message) : action.payload;
      return { ...nextState, requesting: false, error };
    }

    default: {
      /* Return original state if no actions were consumed. */
      return state;
    }
  }
}

module.exports = reducer;
