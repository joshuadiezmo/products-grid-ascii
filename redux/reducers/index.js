import { combineReducers } from "redux";
import products from "./products";

const reducers = {
  products,
};
const combined = combineReducers(reducers);
export default combined;
