import React, { useEffect } from "react";
import PropTypes from "prop-types";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  Table,
} from "reactstrap";


import bootstrap from "../../styles/bootstrap.theme.scss";
import style from "./style.scss";
import getRelativeTime from "../../utils/getRelativeTime";
import Spinner from "../../components/Spinner";
import { DEFAULT_LIMIT } from "../../utils/contants";
import Ads from "../../components/Ads";

const Home = ({
  products, onLoadMore, localPage, ads,
}) => {
  const formatMoney = (price) => {
    const dollar = price / 100;
    return `$${dollar}`;
  };

  const renderItem = (product, index) => (
    <React.Fragment key={`product-${index}`}>
      <Col md={4} sm={6} xs={12} className={bootstrap["mb-3"]}>
        <Card className={bootstrap["h-100"]}>
          <CardBody>
            <div className={[bootstrap["d-flex"], bootstrap["justify-content-center"], bootstrap["mb-3"]].join(" ")}>
              <span style={{ fontSize: product.size }}>
                {product.face}
              </span>
            </div>
            <Table className={style["table-details"]}>
              <tbody>
                <tr>
                  <td><b>ID</b></td>
                  <td>{product.id}</td>
                </tr>
                <tr>
                  <td><b>Size</b></td>
                  <td>{product.size}</td>
                </tr>
                <tr>
                  <td><b>Price</b></td>
                  <td>
                    {formatMoney(product.price)}
                  </td>
                </tr>
                <tr>
                  <td><b>Date</b></td>
                  <td>{getRelativeTime(product.date)}</td>
                </tr>
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </Col>
      {
        (index + 1) % 20 === 0 ? (
          <Col md={4} sm={6} xs={12} className={bootstrap["mb-3"]}>
            <Ads id={ads[Math.ceil(index / 20)]} />
          </Col>
        ) : null
      }
    </React.Fragment>
  );

  const scrolled = () => {
    const container = document.querySelector("#content-container");
    // check if we are already at the bottom of container.
    if (container.scrollTop >= (container.scrollHeight - container.offsetHeight)) {
      onLoadMore();
    }
  };

  const onScroll = () => {
    const container = document.querySelector("#content-container");
    // detect container scroll.
    container.removeEventListener("scroll", scrolled);
    container.addEventListener("scroll", scrolled);
  };

  const renderEndCatalog = () => (
    <h3 className={[
      bootstrap["text-center"],
      bootstrap["py-3"],
    ].join(" ")}
    >
          ~ end of catalogue ~
    </h3>
  );

  useEffect(() => {
    onScroll();
  }, [products]);

  return (
    <Container>
      <Row className={bootstrap["row-eq-height"]}>
        {products.list.slice(0, localPage * DEFAULT_LIMIT).map(renderItem)}
      </Row>
      {products.requesting ? <Spinner /> : null}
      {!products.requesting && !products.nextPage ? renderEndCatalog() : null}
    </Container>
  );
};

Home.defaultProps = {
  products: {},
  localPage: 1,
  ads: [],
};

Home.propTypes = {
  products: PropTypes.object,
  onLoadMore: PropTypes.func.isRequired,
  localPage: PropTypes.number,
  ads: PropTypes.array,
};

export default Home;
