import React from "react";
import {
  Navbar,
  NavbarBrand, NavbarToggler,
} from "reactstrap";
import Link from "next/link";

import bootstrap from "../styles/bootstrap.theme.scss";

const Header = () => (
  <Navbar color="primary" dark expand="md" fixed="top">
    <div className={[bootstrap["d-flex"], bootstrap["align-items-center"]].join(" ")}>
      <NavbarToggler />
      <Link href="/">
        <NavbarBrand href="/" className={bootstrap["ml-3"]}>
            Products Grid
        </NavbarBrand>
      </Link>
    </div>
  </Navbar>
);

export default Header;
