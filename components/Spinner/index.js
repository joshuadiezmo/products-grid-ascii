import React from "react";
import style from "./style.scss";
import bootstrap from "../../styles/bootstrap.theme.scss";

const Spinner = () => (
  <div
    className={[
      bootstrap["d-flex"],
      bootstrap["justify-content-center"],
      bootstrap["align-items-center"],
      bootstrap["my-3"],
    ].join(" ")}
    style={{ height: 30, overflow: "hidden" }}
  >
    <span className={[bootstrap.h3, bootstrap["mr-2"]].join(" ")}>Loading </span>
    <svg className={style.spinner} width="25px" height="25px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
      <circle
        className={style.path}
        fill="none"
        strokeWidth="6"
        strokeLinecap="round"
        cx="33"
        cy="33"
        r="30"
      />
    </svg>

  </div>
);

export default Spinner;
