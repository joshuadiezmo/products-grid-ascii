import React from "react";
import {
  Container, Row, Col, Form, FormGroup, Label, Input,
} from "reactstrap";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import bootstrap from "../../styles/bootstrap.theme.scss";
import styles from "./style.scss";
import { fetchProducts } from "../../redux/actions";
import { SORT } from "../../utils/contants";
import DeveloperPreview from "../DeveloperPreview";

const Content = ({ actions }) => {
  const onSortChange = ({ target }) => {
    actions.fetchProducts({ _page: 1, _sort: target.value });
  };
  return (
    <div className={styles.container}>
      <Container>
        <Row>
          <Col>
            <Form>
              <FormGroup>
                <Label for="sort">Sort</Label>
                <Input type="select" name="sort" id="sort" onChange={onSortChange} defaultValue="default">
                  <option disabled value="default">Select Sort</option>
                  <option value={SORT.SIZE}>Size</option>
                  <option value={SORT.PRICE}>Price</option>
                  <option value={SORT.ID}>ID</option>
                </Input>
              </FormGroup>
              <small className={bootstrap["text-muted"]}>
                  This is for other filter. But on this project, we just need sort.
              </small>
            </Form>
            <DeveloperPreview />
          </Col>
        </Row>
      </Container>
    </div>
  );
};

Content.defaultProps = {
  actions: {},
};

Content.propTypes = {
  actions: PropTypes.object,
};

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  const actions = { fetchProducts };
  const actionMap = {
    actions: bindActionCreators(actions, dispatch),
  };
  return actionMap;
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Content);
