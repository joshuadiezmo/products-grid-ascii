import React, { useState } from "react";
import PropTypes from "prop-types";
import SidebarLib from "react-sidebar";
import Content from "./Content";
import styles from "./style.scss";

const Sidebar = ({ children }) => {
  const [sidebarOpen, setSidebarOpen] = useState(false);
  return (
    <SidebarLib
      sidebar={<Content />}
      open={sidebarOpen}
      onSetOpen={setSidebarOpen}
      shadow={false}
      sidebarClassName={styles.sidebar}
      contentId="content-container"
      docked
    >
      {children}
    </SidebarLib>
  );
};

Sidebar.defaultProps = {
  children: null,
};

Sidebar.propTypes = {
  children: PropTypes.any,
};

export default Sidebar;
