import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import getConfig from "next/config";
import bootstrap from "../styles/bootstrap.theme.scss";

const { publicRuntimeConfig } = getConfig();
const { NODE_ENV } = publicRuntimeConfig;

const DeveloperPreview = ({ products }) => {
  if (NODE_ENV === "development") {
    return (
      <div>
        <hr />
        <small className={bootstrap["text-danger"]}>
          <p>
                    This is Developer View.
            <br />
                    You cannot see this on production build
          </p>
        </small>
        <pre>
          {JSON.stringify({ ...products, list: products.list.length }, null, 2)}
        </pre>
      </div>
    );
  }
  return null;
};


DeveloperPreview.defaultProps = {
  products: {},
};

DeveloperPreview.propTypes = {
  products: PropTypes.object,
};

function mapStateToProps(state) {
  const { products } = state;
  return { products };
}

export default connect(
  mapStateToProps,
)(DeveloperPreview);
