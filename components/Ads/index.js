import React from "react";
import PropTypes from "prop-types";
import { Card, CardBody } from "reactstrap";
import { SERVER } from "../../utils/contants";
import bootstrap from "../../styles/bootstrap.theme.scss";

const Ads = ({ id }) => (
  <div className={bootstrap["h-100"]}>
    <Card className={bootstrap["h-100"]}>
      <CardBody className={[
        bootstrap["bg-dark"],
        bootstrap["d-flex"],
        bootstrap["flex-column"],
        bootstrap["justify-content-between"],
      ].join(" ")}
      >
        <img src={`${SERVER}ads/?r=${id}`} alt={`ads-${id}`} />
        <span className={[bootstrap.h3, bootstrap["text-white"], bootstrap["text-center"]].join(" ")}>
          {`Ad ID - ${id}`}
        </span>
      </CardBody>
    </Card>
  </div>
);

Ads.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
};

export default Ads;
