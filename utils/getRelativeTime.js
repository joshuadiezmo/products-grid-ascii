const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

const plural = value => (value > 1 ? "s" : "");

const getRelativeTime = (param) => {
  const previous = new Date(param);
  const current = new Date();
  const msPerMinute = 60 * 1000;
  const msPerHour = msPerMinute * 60;
  const msPerDay = msPerHour * 24;
  const msPerMonth = msPerDay * 30;
  const elapsed = current.getTime() - previous.getTime();

  let value;

  if (elapsed < msPerMinute) {
    value = Math.round(elapsed / 1000);
    return `${value} second${plural(value)} ago`;
  }

  if (elapsed < msPerHour) {
    value = Math.round(elapsed / msPerMinute);
    return `${value} minute${plural(value)} ago`;
  }

  if (elapsed < msPerDay) {
    value = Math.round(elapsed / msPerHour);
    return `${value} hour${plural(value)} ago`;
  }

  if (elapsed < msPerMonth && Math.round(elapsed / msPerDay) < 8) {
    value = Math.round(elapsed / msPerDay);
    return `${value} day${plural(value)} ago`;
  }

  return `${months[previous.getMonth()]} ${previous.getDate()}, ${previous.getFullYear()}`;
};

export default getRelativeTime;
