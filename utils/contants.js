export const SORT = {
  SIZE: "size",
  PRICE: "price",
  ID: "id",
};

export const DEFAULT_LIMIT = 50;
export const IDLE_TIME = 1 * 5000; // 10 seconds.
export const SERVER = "http://localhost:3000/api/";
