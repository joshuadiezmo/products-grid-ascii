import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { fetchProducts } from "../redux/actions";
import Home from "../screens/Home";
import { DEFAULT_LIMIT, IDLE_TIME } from "../utils/contants";

class Index extends Component {
  state = { localPage: 1, ads: [] };

  componentDidMount() {
    const { actions } = this.props;
    actions.fetchProducts({ _page: 1 });
    this.loadMore();
  }

  componentWillUpdate(nextProps) {
    const { products } = nextProps;
    const { ads } = this.state;
    const divideLength = Math.ceil(products.list.length / 20) + 1;

    while (divideLength > ads.length) {
      const generatedAdId = Math.floor(Math.random() * 10);
      if (ads[ads.length - 1] !== generatedAdId) {
        ads.push(generatedAdId);
        this.setState({ ads }); // eslint-disable-line
      }
    }
  }

  componentWillUnmount() {
    if (this.timeOutLoadMore) clearTimeout(this.timeOutLoadMore);
  }

  // isForce = true if the loadMore called from bottom scroll.
  // isForce = false if the loadMore called from it self.
  loadMore = (isForce) => {
    const { localPage } = this.state;
    const { products } = this.props;

    // clear timeout if the timeOutLoadMore has already value.
    // to prevent multiple calling timeout.
    if (this.timeOutLoadMore) clearTimeout(this.timeOutLoadMore);

    // update localPage if isForce.
    if (isForce && ((localPage - 1) * DEFAULT_LIMIT) < products.list.length) {
      this.setState({ localPage: localPage + 1 });
    }
    this.timeOutLoadMore = setTimeout(() => {
      let { actions, products } = this.props; //eslint-disable-line
      const { nextPage, _sort } = products;
      if (products.nextPage && !products.requesting) {
        actions.fetchProducts({ _page: nextPage, _sort });
      }
      if (nextPage) this.loadMore();
    }, isForce ? 0 : IDLE_TIME); // force to run timeout if isForce.
  };

  render() {
    const { localPage, ads } = this.state;
    const { products } = this.props;
    return (
      <Home
        products={products}
        onLoadMore={() => this.loadMore(true)}
        localPage={localPage}
        ads={ads}
      />
    );
  }
}

Index.defaultProps = {
  products: {},
  actions: {},
};

Index.propTypes = {
  products: PropTypes.object,
  actions: PropTypes.shape({
    fetchProducts: PropTypes.func,
  }),
};

function mapStateToProps(state) {
  const { products } = state;
  return { products };
}

function mapDispatchToProps(dispatch) {
  const actions = { fetchProducts };
  const actionMap = {
    actions: bindActionCreators(actions, dispatch),
  };
  return actionMap;
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Index);
