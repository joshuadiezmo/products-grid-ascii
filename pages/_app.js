import React from "react";
import App, { Container } from "next/app";
import Head from "next/head";
import { Provider } from "react-redux";
import withRedux from "next-redux-wrapper";
import { Util } from "reactstrap";
import configureStore from "../redux/store";
import Header from "../components/Header";
import bootstrap from "../styles/bootstrap.theme.scss";
import styles from "../styles/_app.scss";
import Sidebar from "../components/Sidebar";

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps, store } = this.props;
    Util.setGlobalCssModule(bootstrap);
    return (
      <Provider store={store} id="app">
        <Container>
          <Head>
            <meta charSet="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            <title>
              Products Grid
            </title>
          </Head>
          <Sidebar>
            <Header />
            <div className={styles.container}>
              <Component {...pageProps} />
            </div>
          </Sidebar>
        </Container>
      </Provider>
    );
  }
}

export default withRedux(configureStore)(MyApp);
