How to run
====
1. clone the project
2. cd to project folder
3. run `npm install`
4. run `npm start` to start api server
5. run `npm run dev` fror frontend dev
6. run `npm run build` to build production bundle
7. run `npm run start:frontend` to start production frontend server
