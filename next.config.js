// next.config.js
const withSass = require("@zeit/next-sass");

module.exports = withSass({
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[hash:base64:5]",
  },
  publicRuntimeConfig: {
    NODE_ENV: process.env.NODE_ENV,
  },
});
